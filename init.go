package main

import (
    "flag"
)

var (
    argFilename   string
    optOutputFile string
    optHelp       bool
    optVerbose    bool
)

// Initializes the program and parses command-line arguments and options. 
// Called before the main function to separate argument parsing from core
// application logic.
func init() {
    flag.BoolVar(&optHelp, "help", false, "Request help documentation")
    flag.BoolVar(&optHelp, "h", optHelp, "Request help documentation")
    flag.BoolVar(&optHelp, "?", optHelp, "Request help documentation")
    flag.BoolVar(&optVerbose, "verbose", false, "Enables verbose logging")
    flag.BoolVar(&optVerbose, "v", optVerbose, "Enables verbose logging")
    flag.StringVar(&optOutputFile, "output", "", "Output file for the data map")
    flag.StringVar(&optOutputFile, "o", optOutputFile, "Output file for the data map")
    flag.Usage = help
    flag.Parse()

    if !flag.Parsed() || optHelp || len(flag.Args()) < 1 { flag.Usage() }
    argFilename = flag.Arg(0)
    if optOutputFile == "" {
        optOutputFile = argFilename
    }
}